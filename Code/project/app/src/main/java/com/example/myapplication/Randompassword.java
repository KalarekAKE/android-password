package com.example.myapplication;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.Nullable;

import java.util.Random;

public class Randompassword extends Fragment {
    int maxlength = 8;
    View viewpage;
    EditText editTextRandom;
    TextView tvProgressLabel ;
    String group = "";
    private ClipboardManager myClipboard;
    private ClipData myClip;
    TextView textView;
    Button copyText;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        viewpage = inflater.inflate(R.layout.randompassword, container, false);
        editTextRandom = (EditText) viewpage.findViewById(R.id.editTextRandom);
        editTextRandom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        SeekBar seekBar = viewpage.findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        int progress = seekBar.getProgress();
        String pro = Integer.toString(progress);
        tvProgressLabel = viewpage.findViewById(R.id.seektext);
        tvProgressLabel.setText(pro);



        editTextRandom = viewpage.findViewById(R.id.editTextRandom);
        // Function to generate random alpha-numeric password of specific length
        final Button button1 = viewpage.findViewById(R.id.buttonRandom);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                group = generateRandomPassword(maxlength,true,true,true,true);
                editTextRandom.setText(group);
            }
        });

        final Button btcopyText = viewpage.findViewById(R.id.btcopyText);
        btcopyText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(editTextRandom.getText().length() != 0) {
                    ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText(null, editTextRandom.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getActivity().getApplicationContext(),"Copy to Clipboard",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return viewpage;
    }
    private static String generateRandomPassword(int max_length, boolean upperCase, boolean lowerCase, boolean numbers, boolean specialCharacters)
    {
        String upperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseChars = "abcdefghijklmnopqrstuvwxyz";
        String numberChars = "0123456789";
        String specialChars = "!@#$%^&*()_-+=<>?/{}~|";
        String allowedChars = "";

        Random rn = new Random();
        StringBuilder sb = new StringBuilder(max_length);

        //this will fulfill the requirements of atleast one character of a type.
        if(upperCase) {
            allowedChars += upperCaseChars;
            sb.append(upperCaseChars.charAt(rn.nextInt(upperCaseChars.length()-1)));
        }

        if(lowerCase) {
            allowedChars += lowerCaseChars;
            sb.append(lowerCaseChars.charAt(rn.nextInt(lowerCaseChars.length()-1)));
        }

        if(numbers) {
            allowedChars += numberChars;
            sb.append(numberChars.charAt(rn.nextInt(numberChars.length()-1)));
        }

        if(specialCharacters) {
            allowedChars += specialChars;
            sb.append(specialChars.charAt(rn.nextInt(specialChars.length()-1)));
        }


        //fill the allowed length from different chars now.
        for(int i=sb.length();i < max_length;++i){
            sb.append(allowedChars.charAt(rn.nextInt(allowedChars.length())));
        }

        return  sb.toString();
    }
    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // updated continuously as the user slides the thumb
            String pro = Integer.toString(progress);
            tvProgressLabel.setText(pro);
            maxlength = progress;

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // called when the user first touches the SeekBar
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // called after the user finishes moving the SeekBar
        }
    };

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
