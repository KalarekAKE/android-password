package com.example.myapplication;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.List;

public class Check_Crack {
    private String passwordtext ,powsTime = "",perc ="";
    final double a =93;
    private int progr=0;
    String complex = "";
    String sa , sTime,valuepow;
    int gtext =0 ;
    public int sendCalculate(int timeSecond , double guesses ,double guessesP,int passwordLength){
        double sum = 0;
        double   sumPasswordTime = (timeSecond*(guesses*Math.pow(10,guessesP)))/Math.pow(passwordLength,a);
        sa = String.valueOf(sumPasswordTime);
        sTime = sa.substring(0,5);
        powsTime = "";
        complex = "";
        char ch = 0;
        int i=0;
        for(char c : sa.toCharArray()){
            i++;
            if(c == 'E'){
                powsTime = sa.substring(i);
            }
        }
        complex = (powsTime);

        valuepow = String.valueOf(complex);
        String pow ;
        if (valuepow.length() == 3) {
            pow = valuepow.substring(1, 3);
        }else{
            pow = valuepow.substring(1, 4);
        }
        gtext = 0 ;
        int MyNumber = Integer.parseInt(pow);
        progr = 0;
        if(MyNumber <= 42 ){
            gtext = 1;
        }
        else if(MyNumber <= 54){
            gtext = 2;
        }
        else if(MyNumber <= 66){
            gtext = 3;
        }
        else if(MyNumber <= 78){
            gtext = 4;
        }
        else if (MyNumber > 78 || MyNumber > 100){
            gtext = 5;
        }else{
            gtext = 0;
        }

        String log = "Id: " + gtext;
        Log.d("Name: ", log);
        return gtext;
    }


    public int progressNum(){
        String valuepow = String.valueOf(complex);
        String pow ;
        if (valuepow.length() == 3) {
            pow = valuepow.substring(1, 3);
        }else{
            pow = valuepow.substring(1, 4);
        }
        Integer gtext = 0 ;
        int MyNumber = Integer.parseInt(pow);
        progr = 0;
        if(MyNumber <= 42 ){
            progr = 20;
        }
        else if(MyNumber <= 54){
            progr = 40;
        }
        else if(MyNumber <= 66){
            progr = 60;
        }
        else if(MyNumber <= 78){
            progr = 80;
        }
        else if (MyNumber > 78 || MyNumber > 100){
            progr = 100;
        }else{
            gtext = 0;
        }

        String log = "Id: " + gtext;
        Log.d("Name: ", log);
        return progr;
    }

    public String progressText(){
        String valuepow = String.valueOf(complex);
        String pow ;
        if (valuepow.length() == 3) {
            pow = valuepow.substring(1, 3);
        }else{
            pow = valuepow.substring(1, 4);
        }
        Integer gtext = 0 ;
        int MyNumber = Integer.parseInt(pow);
        perc = "";
        if(MyNumber <= 42 ){
            perc = "Terrible";
        }
        else if(MyNumber <= 54){
            perc = "Weak";
        }
        else if(MyNumber <= 66){
            perc = "Good";
        }
        else if(MyNumber <= 78){
            perc = "Strong";
        }
        else if (MyNumber > 78 || MyNumber > 100){
            perc = "Perfect";
        }
        return perc;
    }

    public String getCrack(int timeSecond , double guesses ,double guessesP,int passwordLength){
        double   sumPasswordTime = (timeSecond*(guesses*Math.pow(10,guessesP)))/Math.pow(passwordLength,a);
        sa = String.valueOf(sumPasswordTime);
        sTime = sa.substring(0,5);
        powsTime = "";
        complex = "";
        char ch = 0;
        int i=0;
        for(char c : sa.toCharArray()){
            i++;
            if(c == 'E'){
                powsTime = sa.substring(i);
            }
        }
        String dataCrack = sTime+"x 10^"+powsTime;
        return dataCrack;
    }
}
