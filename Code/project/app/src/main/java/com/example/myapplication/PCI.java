package com.example.myapplication;

import android.util.Log;

import java.sql.Timestamp;
import java.util.List;

public class PCI {
    public double pciClass(int mLength,int numCase,int symbolCase,int textsLower,int textsUpper){
        double pciCalculat =0;
        pciCalculat = mLength * 10*2;

        if (numCase > 0 && symbolCase > 0 && textsLower > 0 && textsUpper > 0) { // 93 ___lower,Upper,number && symbol
            pciCalculat = mLength * Math.log10(93);
        } else if (numCase > 0 && symbolCase >0 && textsLower >0)
        {  //pow 67 ___lower,number && symbol
            pciCalculat = mLength * Math.log10(67);

        }else if (numCase > 0 && symbolCase >0 && textsUpper >0)
        { // pow 67 ____Upper,number && symbol
            pciCalculat = mLength * Math.log10(67);
        }else if (textsLower > 0 && numCase >0 && textsUpper >0)
        { //pow 67 ____Upper,Lower && number
            pciCalculat = mLength * Math.log10(97);

        }else if (textsLower > 0 && symbolCase >0 && textsUpper >0)
        {// pow 67 ____Upper,Lower && symbol
            pciCalculat = mLength * Math.log10(67);

        }else if (textsLower > 0 && numCase >0){ // pow 57 ___Lower && num
            pciCalculat = mLength * Math.log10(57);

        }else if (textsLower > 0 && symbolCase >0 ){ // pow 57 ____Lower && symbol
            pciCalculat = mLength * Math.log10(57);

        }else if (textsUpper > 0 && numCase > 0 ){ // pow 57 ___Upper && num
            pciCalculat = mLength * Math.log10(57);

        }else if (textsUpper > 0 && symbolCase > 0){ // pow 57 ____Upper && symbol
            pciCalculat = mLength * Math.log10(57);

        }else if (numCase > 0 && symbolCase > 0){ //pow 41 _____num && symbol
            pciCalculat = mLength * Math.log10(41);

        }else if (textsLower > 0 && textsUpper > 0){ // pow 52 _____Lower && Upper
            pciCalculat = mLength * Math.log10(52);

        }else{
            if (symbolCase > 0) {//pow 31
                pciCalculat = mLength * Math.log10(31);
            } else if (numCase > 0) { //pow 10
                pciCalculat = mLength * Math.log10(10);
            } else if (textsUpper > 0 || textsLower > 0) { //pow 26
                pciCalculat = mLength * Math.log10(26);
            }
        }
        return pciCalculat;
    }

    public int levelEffectL(double length){
        int len = (int)length;
        Log.d("GG","GGGGGG"+len);
        int levelEffLen =0;
        if(len < 0){
            levelEffLen =1;
        }else if(len <= 2){
            levelEffLen =1;
        }else if(len <= 5){
            levelEffLen =2;
        }else if(len <= 7){
            levelEffLen =3;
        }else if(len <= 14){
            levelEffLen =4;
        }else {
            levelEffLen =5;
        }
        return levelEffLen;
    }

}
