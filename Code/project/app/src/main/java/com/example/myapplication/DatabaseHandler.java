package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "checkStrength";
    // Contacts table name
    private static final String TABLE_CHECKSTRENGTH = "strengths";
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_STRENGTH = "strength";
    private static final String KEY_TIME = "time";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CHECKSTRENGTH_TABLE = "CREATE TABLE " + TABLE_CHECKSTRENGTH + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_STRENGTH + " INTEGER,"
                + KEY_TIME + " TEXT" + ")";
        db.execSQL(CREATE_CHECKSTRENGTH_TABLE);
    }

    // Upgrading database (drop older table if existed, & create table again)
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHECKSTRENGTH);
        onCreate(db);
    }
    public void addStrength(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_STRENGTH, contact.getStrength());
        values.put(KEY_TIME, contact.getTimestamp());

        // Inserting Row
        db.insert(TABLE_CHECKSTRENGTH, null, values);
        db.close(); // Closing database connection
    }

    public Contact getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHECKSTRENGTH, new String[] { KEY_ID,
                        KEY_STRENGTH, KEY_TIME }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getInt(1), cursor.getString(2));
        // return contact
        return contact;
    }
    // Getting All Contacts
    public List<Contact> getAllContacts() {
        List<Contact> contactList = new ArrayList<Contact>();
        String selectQuery = "SELECT * FROM " + TABLE_CHECKSTRENGTH;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setStrength(cursor.getInt(1));
                contact.setTimestamp(cursor.getString(2));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        // return contact list
        return contactList;
    }
    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT * FROM " + TABLE_CHECKSTRENGTH;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }
    // Updating single contact
    public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_STRENGTH, contact.getStrength());
        values.put(KEY_TIME, contact.getTimestamp());
        // updating row
        return db.update(TABLE_CHECKSTRENGTH, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
    }
    // Deleting single contact
    public void deleteContact() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CHECKSTRENGTH,null,null);
        db.close();
        Bundle args = new Bundle();
        args.putBoolean("Key Answer", true);
        Cal_Password f = new Cal_Password();
        f.setArguments(args);
    }
}