package com.example.myapplication;

import android.util.Log;

import java.sql.Timestamp;
import java.util.List;

public class Entropy {
    private int dataG;
    public void saveEntropy(){
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        dbCheckStrength dbcheckstrength = new dbCheckStrength();
        DatabaseCheckStrength db = new DatabaseCheckStrength(null);
        int aStringdataG = dataG;

        // Inserting Contacts
        Log.d("Insert: ", "Inserting ..");
        db.adddbCheckStrength(new dbCheckStrength(aStringdataG, timestamp.toString()));

        // Reading all contacts
        Log.d("Reading: ", "Reading all contacts..");
        List<dbCheckStrength> bdcheck = db.getAlldbCheckStrengths();
        for (dbCheckStrength cn : bdcheck) {
            String log = "Id: " + cn.getID() + " ,Strength: " + cn.getStrength() +
                    " ,Time: " + cn.getTimestamp();
            Log.d("Name: ", log);
        }
    }

    static long factorials(int n) {
        long fact = 1;
        for (int i = 2; i <= n; i++)
            fact = fact * i;
        return fact;
    }

    public double pow(int n , int m) {
        double mathpow = Math.pow(n,m);
        return mathpow;
    }

    public double main(int textsLower, int textsUpper, int lengths, int symbolCase, int numCase) {
        double compaxcityo = 0, compaxcityt =0, compaxcity =0, passwordStrength =0x11L;
        String dataText = "";
        if (numCase > 0 && symbolCase > 0 && textsLower > 0 && textsUpper > 0){ // 93 ___lower,Upper,number && symbol
            int lengthT = lengths - 1;
            int lengthTh =  lengthT - 1;
            compaxcityo = (factorials(lengths) / (factorials(textsUpper) * factorials(lengths-textsUpper)));
            compaxcityt = (factorials(lengthT) / (factorials(numCase) * factorials(lengthT-numCase)));
            compaxcity = (factorials(lengthTh) / (factorials(symbolCase) * factorials(lengthTh-symbolCase)));
            passwordStrength = ((pow(26 , textsLower) * compaxcityo * pow(26 , textsUpper) * compaxcityt * pow(10 , numCase) * compaxcity * pow(31,symbolCase)));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "1111111111111111111111111111";


        }else if (numCase > 0 && symbolCase > 0 && textsLower > 0){ // pow 67 ___lower,number && symbol
            compaxcityo = (factorials(lengths) / (factorials(numCase) * factorials(lengths-numCase)));
            compaxcityt = (factorials(lengths-1) / (factorials(symbolCase) * factorials((lengths - 1)-symbolCase)));
            passwordStrength =(pow(26 , textsLower) * compaxcityo * pow(10 , numCase) * compaxcityt * pow(31,symbolCase));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "2222222222222222222222222222222222222";

        }else if (numCase > 0 && symbolCase > 0 && textsUpper > 0){ // pow 67 ____Upper,number && symbol
            compaxcityo = (factorials(lengths) / (factorials(numCase) * factorials(lengths-numCase)));
            compaxcityt = (factorials(lengths-1) / (factorials(symbolCase) * factorials((lengths - 1)-symbolCase)));
            passwordStrength = (pow(26 , textsUpper) * compaxcityo * pow(10 , numCase) * compaxcityt * pow(31,symbolCase));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "33333333333333333333333";

        }else if (textsLower > 0 && numCase > 0 && textsUpper > 0){ // pow 67 ____Upper,Lower && number
            compaxcityo = (factorials(lengths) / (factorials(numCase) * factorials(lengths-numCase)));
            compaxcityt = (factorials(lengths-1) / (factorials(textsLower) * factorials((lengths - 1)-textsLower)));
            passwordStrength = (pow(26 , textsUpper) * compaxcityo * pow(10 , numCase) * compaxcityt * pow(26,textsLower));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "444444444444444444444";

        }else if (textsLower > 0 && symbolCase > 0 && textsUpper > 0){ // pow 67 ____Upper,Lower && symbol
            compaxcityo = (factorials(lengths) / (factorials(textsLower) * factorials(lengths-textsLower)));
            compaxcityt = (factorials(lengths-1) / (factorials(symbolCase) * factorials((lengths - 1)-symbolCase)));
            passwordStrength = (pow(26 , textsUpper) * compaxcityo * pow(26, textsLower) * compaxcityt * pow(31,symbolCase));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "555555555555555555555555";;

        }else if (textsLower > 0 && numCase > 0 ){ // pow 57 ___Lower && num
            compaxcityo = (factorials(lengths) / (factorials(textsLower) * factorials(lengths-textsLower)));
            passwordStrength = (pow(10,numCase)*compaxcityo*pow(26,textsLower));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase+ "666666666666666666666";

        }else if (textsLower > 0 && symbolCase > 0){ // pow 57 ____Lower && symbol
            compaxcityo = (factorials(lengths) / (factorials(textsLower) * factorials(lengths-textsLower)));
            passwordStrength = (pow(31,symbolCase)*compaxcityo*pow(26,textsLower));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase+ "777777777777777777777";

        }else if (textsUpper > 0 && numCase > 0 ){ // pow 57 ___Upper && num
            compaxcityo = (factorials(lengths) / (factorials(textsUpper) * factorials(lengths-textsUpper)));
            passwordStrength = (pow(10,numCase)*compaxcityo*pow(26,textsUpper));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase+ "8888888888888888888888";

        }else if (textsUpper > 0 && symbolCase > 0){ // pow 57 ____Upper && symbol
            compaxcityo = (long)(factorials(lengths) / (factorials(textsUpper) * factorials(lengths-textsUpper)));
            passwordStrength = (pow(31,symbolCase)*compaxcityo*pow(26,textsUpper));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase+ "999999999999999999999";

        }else if (numCase > 0 && symbolCase > 0){ // pow 41 _____num && symbol
            compaxcityo = (long)(factorials(lengths) / (factorials(symbolCase) * factorials(lengths-symbolCase)));
            passwordStrength = (pow(10,numCase)*compaxcityo*pow(31,symbolCase));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase+ "1010101010101010101010101010";

        }else if (textsLower > 0 && textsUpper > 0){ // pow 41 _____Lower && Upper
            compaxcityo = (long)(factorials(lengths) / (factorials(textsLower) * factorials(lengths-textsLower)));
            passwordStrength = (pow(26,textsUpper)*compaxcityo*pow(26,textsLower));
            dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "00000000000000000000000000000000000000000000";;

        }else{
            if (symbolCase > 0){ //pow 31
                passwordStrength = (pow(31,symbolCase));
                dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "----------------------------";;
            }else if (numCase > 0){ //pow 10
                passwordStrength = (pow(10,numCase));
                dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase + "++++++++++++++++++++++++++++++++";;
            }else if (textsUpper > 0){ //pow 26
                passwordStrength = (pow(26,textsUpper));
                dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase+ "********************************";;
            }else if (textsLower > 0){ //pow 26
                passwordStrength = (pow(26,textsLower));
                dataText = "Lower" + textsLower + "Upper" + textsUpper + "Number" + numCase + "symbol"+symbolCase+ "///////////////////////////////";;
            }
        }
        return passwordStrength;
    }


    public int dataGraph(int lens, double cal){
        int length = lens;
        double dataCal = cal;
        int dataGrap= 0;

        if (length == 4) {
            dataGrap = checkRange4_5(dataCal);
        }else if (length == 5) {
            dataGrap = checkRange4_5(dataCal);
        }else if (length == 6) {
            dataGrap = checkRange4_5(dataCal);
        }else if (length == 7) {
            dataGrap = checkRange4_5(dataCal);
        }else if (length == 8) {
            dataGrap = checkRange11(dataCal);
        }else if (length == 9) {
            dataGrap = checkRange11(dataCal);
        }else if (length == 10) {
            dataGrap = checkRange11(dataCal);
        }else if (length == 11) {
            dataGrap = checkRange11(dataCal);
        }else if (length == 12) {
            dataGrap = checkRange11(dataCal);
        }else if (length == 13) {
            dataGrap = checkRange11(dataCal);
        }else if (length == 14) {
            dataGrap = checkRange11(dataCal);
        }else if (length == 15) {
            dataGrap = checkRange11(dataCal);
        }else if(length >= 16) {
            dataGrap = checkRange11(dataCal);
        }else{
            dataGrap = 0;
        }

        return dataGrap;
    }

    public int checkRange4_5(double dataP){
        int dataGraph = 0;
        if (dataP > 4) {
            dataGraph = 2;
        }else if(dataP >= 2){
            dataGraph = 2;
        }else {
            dataGraph = 1;
        }
        return dataGraph;
    }

    public int checkRange11(double dataP) {
        int dataGraph = 0;
        if (dataP >5){
            dataGraph = 5;
        }else if (dataP >= 5) {
            dataGraph = 4;
        }else if (dataP >= 0.89) {
            dataGraph = 3;
        }else if (dataP >= 0.7) {
            dataGraph = 2;
        } else {
            dataGraph = 1;
        }
        return dataGraph;
    }

    public String dataEntropy(double entro){
        String entropyData ="" ,sa = "",sEntro ="", powsEntro ="";
        sa = String.valueOf(entro);
        sEntro = sa.substring(0,5);
        powsEntro = "";
        char ch = 0;
        int i=0;
        for(char c : sa.toCharArray()){
            i++;
            if(c == 'E') {
                powsEntro = sa.substring(i);
                break;
            }else {
                powsEntro = "1";
            }
        }
        if( powsEntro.equals("1")){
            entropyData = sa;
        }else {
            entropyData = sEntro + "x 10^" + powsEntro;
        }
            return entropyData;

    }
}
