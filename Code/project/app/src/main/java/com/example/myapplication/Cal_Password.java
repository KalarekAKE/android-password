package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import org.jetbrains.annotations.Nullable;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Cal_Password extends Fragment {
    View viewpage;
    private RadarChart chart;
    int lower_case = 0,upper_case = 0 ,numbers_case =0,symbols_case =0,passwordLength =0,timeSecond=0, pos = 0, pos2 = 0;
    private String passwordtext ,powsTime = "",perc ="" , dataCarck="",dataEntro="";
    TextView Plength,PLower,PUpper,PNumbers,PSymbols,complexity,status,comple;
    EditText editTextTextPassword;
    final double a =93;
    double passStrength=0,guesses=0,guessesP=0;
    private int progrsC=0;
    private ProgressBar progressBar;
    private int dataG,dataproEffL,dataproTime;
    private static DecimalFormat df2 = new DecimalFormat("#.##");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewpage = inflater.inflate(R.layout.check_hack_activity, container, false);
        editTextTextPassword = viewpage.findViewById(R.id.editTextTextPassword);
        Spinner spinner = (Spinner) viewpage.findViewById(R.id.spinner);
        Spinner spinner2 = (Spinner) viewpage.findViewById(R.id.spinner2);
        editTextTextPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        Bundle bundle = new Bundle();
        Bundle bundleData = this.getArguments();

        Chart(lower_case, upper_case, numbers_case, symbols_case, passwordLength);
        MainActivity ma = new MainActivity();

        final Button button1 = viewpage.findViewById(R.id.button01);

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try{
                    // Call Class Check_Crack
                    Check_Crack crack = new Check_Crack();
                    dataCarck = crack.getCrack(timeSecond,guesses,guessesP,passwordLength);
                    progrsC = crack.sendCalculate(timeSecond,guesses,guessesP,passwordLength);
                    perc = crack.progressText(); // Full

                    Entropy entro = new Entropy();
                    double dataCompax = entro.main(lower_case,upper_case,passwordLength, symbols_case, numbers_case);
                    double vuu = dataCompax/(double)Math.pow(93,passwordLength);
                    dataEntro = entro.dataEntropy(dataCompax);
                    dataG = entro.dataGraph(passwordLength,vuu*100); //Full

                    // Call Class PCI
                    PCI calPci = new PCI();
                    double ansPCi = calPci.pciClass(passwordLength,numbers_case,symbols_case,lower_case,upper_case);
                    dataproEffL = calPci.levelEffectL(ansPCi);
                    Log.d("PCI"," PCI============= "+ansPCi);

                    // Call Class Check_Cracktime
                    Check_Cracktime crackTime = new Check_Cracktime();
//                    double carckData = crackTime.Cracktime(passwordLength,guesses,guessesP);
                    String carckTimeString = crackTime.Cracktime_toString(passwordLength,guesses,guessesP);
                    dataproTime = crackTime.dataLevel();



                    // Start Save DataBase _____________
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                    Contact contact = new Contact();
                    DatabaseHandler db = new DatabaseHandler(getActivity());
                    // Inserting Contacts
                    int A = progrsC;
                    Log.d("Insert: ", "Inserting ..");
                    db.addStrength(new Contact(A, timestamp.toString()));

                    // Reading all contacts
                    Log.d("Reading: ", "Reading all contacts..");
                    List<Contact> contacts = db.getAllContacts();
                    for (Contact cn : contacts) {
                        String log = "Id: " +cn.getID()+ " ,Strength: " + cn.getStrength() +
                                " ,Time: " + cn.getTimestamp();
                        Log.d("Name: ", log);
                    }


                    dbCheckStrength dbcheckstrength = new dbCheckStrength();
                    DatabaseCheckStrength dbs = new DatabaseCheckStrength(getActivity());
                    int aStringdataG = dataG;

                    // Inserting Contacts
                    Log.d("Insert: ", "Inserting ..");
                    dbs.adddbCheckStrength(new dbCheckStrength(aStringdataG, timestamp.toString()));
                    // End Save DataBase _____________
                    Show_Detils showDetils = new Show_Detils();
                    bundle.putInt("key_name1", progrsC);
                    bundle.putInt("key_name2", dataG);
                    bundle.putInt("key_name3", dataproEffL);
                    bundle.putInt("key_name4", dataproTime);
                    bundle.putString("key_name5", perc);
                    bundle.putDouble("key_name6", dataCompax);
                    bundle.putDouble("key_name7", vuu);
                    bundle.putDouble("key_name8", ansPCi);
//                    bundle.putString("key_name9", editTextTextPassword.getText().toString());
                    bundle.putString("key_name10", dataCarck);
                    bundle.putString("key_name11", dataEntro);
                    bundle.putString("key_name12", carckTimeString);
                    bundle.putString("key_name13", passwordtext);
                    bundle.putInt("key_name14", pos);
                    bundle.putInt("key_name15", pos2);

                    showDetils.setArguments(bundle);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, showDetils)
                            .commit();
                }catch(Exception error1){
                    String err = error1.toString();
                    Log.d("Error",err);
                    if(err.equals("java.lang.StringIndexOutOfBoundsException: length=3; index=5")){
                        Toast.makeText(getActivity().getApplicationContext(), "Please choose computer speed and time use password", Toast.LENGTH_LONG).show();
                    }else if(err.equals("java.lang.StringIndexOutOfBoundsException: length=0; index=4")){
                        Toast.makeText(getActivity().getApplicationContext(), "Please fill in your password", Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getActivity().getApplicationContext(), "Please change your password", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });




        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.planets_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Spinner spinner = (Spinner) viewpage.findViewById(R.id.spinner);
                pos = spinner.getSelectedItemPosition();
                if (pos == 0){
                    guesses= 0;
                    guessesP=0;
                }else if (pos == 1){
                    guesses= 624;
                    guessesP = 15;
                }else if(pos == 2){
                    guesses= 420;
                    guessesP=15;
                }else if (pos == 3){
                    guesses= 130;
                    guessesP= 15;
                }else if (pos == 4){
                    guesses= 34.32;
                    guessesP= 15;
                }else if (pos == 5){
                    guesses= 1.39;
                    guessesP= 15;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.planets_month, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Spinner spinner2 = (Spinner) viewpage.findViewById(R.id.spinner2);
                pos2 = spinner2.getSelectedItemPosition();
                if (pos2 == 0){
                    timeSecond = 0;
                }else if (pos2 == 1){
                    timeSecond = 60*60*24*1;
                }else if(pos2 == 2){
                    timeSecond = 60*60*24*7;
                }else if (pos2 == 3){
                    timeSecond = 60*60*24*30;
                }else if (pos2 == 4){
                    timeSecond = 60*60*24*90;
                }else if (pos2 == 5){
                    timeSecond = 60*60*24*180;
                }else if (pos2 == 6){
                    timeSecond = 60*60*24*365;
                }else{
                    timeSecond= 100;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        editTextTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                passwordtext = editTextTextPassword.getText().toString();
                passwordLength =  passwordtext.length();
                int low=0,up=0,num=0,sym=0;

                for(char c : passwordtext.toCharArray()) {
                    if(Character.isLowerCase(c)){
                        low++;
                        lower_case = low;
                    }
                    else if(Character.isUpperCase(c)){
                        up++;
                        upper_case = up;
                    }
                    else if(Character.isDigit(c)){
                        num++;
                        numbers_case = num;
                    }
                    else{
                        sym++;
                        symbols_case = sym;
                    }
                }
                Integer l = new Integer(low);
                Integer u = new Integer(up);
                Integer n = new Integer(num);
                Integer sy = new Integer(sym);
                Chart(lower_case, upper_case, numbers_case, symbols_case, passwordLength);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        if (bundleData != null) {
            passwordtext=bundleData.getString("key1",""); //0 is the default value , you can change that
            pos = bundleData.getInt("key2",0);
            pos2 = bundleData.getInt("key3",0);
            editTextTextPassword.setText(passwordtext);
            spinner.setSelection(pos);
            spinner2.setSelection(pos2);
            Log.d("Name: ", "_______________________"+ "_________" +passwordtext + pos +"________" +pos2);
        }
        return viewpage;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void Chart(int lower_case, int upper_case, int numbers_case, int symbols_case, int passwordLength){
        chart = viewpage.findViewById(R.id.radarChart);
        chart.setBackgroundColor(Color.rgb(255, 255, 255));

        chart.getDescription().setEnabled(false);
        chart.setScaleY(1f);
        chart.setScaleX(1f);
        chart.setWebLineWidth(1f);
        chart.setWebColor(Color.LTGRAY);
        chart.setWebLineWidthInner(2f);
        chart.setWebColorInner(Color.LTGRAY);
        chart.setWebAlpha(200);

        RadarChart radarChart = viewpage.findViewById(R.id.radarChart);
        ArrayList<RadarEntry> visitorsForFirstWebsite = new ArrayList<>();
        visitorsForFirstWebsite.add(new RadarEntry(upper_case));
        visitorsForFirstWebsite.add(new RadarEntry(lower_case));
        visitorsForFirstWebsite.add(new RadarEntry(symbols_case));
        visitorsForFirstWebsite.add(new RadarEntry(numbers_case));
        visitorsForFirstWebsite.add(new RadarEntry(passwordLength));


        RadarDataSet radarDataSetForFirstWebsite = new RadarDataSet(visitorsForFirstWebsite,"");
        radarDataSetForFirstWebsite.setColor(Color.rgb(255, 255, 255));
        radarDataSetForFirstWebsite.setFillColor(Color.rgb(103, 110, 129));
        radarDataSetForFirstWebsite.setValueTextColor(Color.rgb(0,0,0));
        radarDataSetForFirstWebsite.setValueTextSize(15);
        radarDataSetForFirstWebsite.setDrawFilled(true);
        radarDataSetForFirstWebsite.setFillAlpha(90);
        radarDataSetForFirstWebsite.setLineWidth(2f);
        radarDataSetForFirstWebsite.setDrawHighlightCircleEnabled(true);
        radarDataSetForFirstWebsite.setDrawHighlightIndicators(false);

        RadarData radarData = new RadarData();
        radarData.addDataSet(radarDataSetForFirstWebsite);

        //------------------------------------------------------

        //------------------------------------------------------

        String[] lebels = {"UpperCase", "LowerCase", "Symbol", "Number", "Length"};
        chart.animateXY(1000, 1000, Easing.EaseInOutQuad);

        XAxis xAxis = chart.getXAxis();
        xAxis.setLabelCount(5, true);
        xAxis.setDrawLabels(true);
        xAxis.setTextSize(14f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum(15f);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(lebels));
        xAxis.setTextColor(Color.BLACK);

        YAxis yAxis = chart.getYAxis();
        yAxis.setLabelCount(5, true);
        yAxis.setDrawLabels(true);
        yAxis.setTextSize(14f);
        yAxis.setYOffset(0f);
        yAxis.setXOffset(0f);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(15f);
        yAxis.setTextColor(Color.BLACK);

        Legend l = chart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.WHITE);

        radarChart.setData(radarData);
    }
}
