package com.example.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Random;

import static java.lang.Math.random;

public class Show_Detils extends Fragment {
    View viewpage;
    private RadarChart chart;
    ProgressBar progreTime , progreComple,progrePCI,progreEntro;
    TextView progTextC,progTextT,progTextP,progTextE,nameC,nameT,nameE,nameP,calEP,calEF,calT,calCP,textPass;
    int times =0,ef=0,compex=0,entropy=0,progTime=0,progEF =0,progCP =0,progEP =0, pos = 0, pos2 = 0;
    String timeText ="" , compleText ="", pciText ="",entroText ="",perCP="",dataCa="",dataEntro="",timeSring = "",dataPassword="";
    double dataFullEp,effect,crateTime,dataEp;
    Button back;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewpage = inflater.inflate(R.layout.redar_chart_detail, container, false);
        super.onCreate(savedInstanceState);
        Cal_Password cal = new Cal_Password();
        MainActivity ma = new MainActivity();
        Bundle bundle = this.getArguments();
        Bundle bundleData = new Bundle();
        back = viewpage.findViewById(R.id.back);
        textPass = viewpage.findViewById(R.id.textpass);
        if (bundle != null) {
            pos=bundle.getInt("key_name14",0); //0 is the default value , you can change that
            pos2=bundle.getInt("key_name15",0);
            compex=bundle.getInt("key_name1",0); //0 is the default value , you can change that
            entropy=bundle.getInt("key_name2",0);
            ef=bundle.getInt("key_name3",0); //0 is the default value , you can change that
            times=bundle.getInt("key_name4",0);
            perCP=bundle.getString("key_name5",""); //0 is the default value , you can change that
            dataCa=bundle.getString("key_name10",""); //0 is the default value , you can change that
            dataEntro=bundle.getString("key_name11",""); //0 is the default value , you can change that
            timeSring=bundle.getString("key_name12",""); //0 is the default value , you can change that
            dataPassword=bundle.getString("key_name13",""); //0 is the default value , you can change that
            dataEp=bundle.getDouble("key_name6",0);
            dataFullEp=bundle.getDouble("key_name7",0); //0 is the default value , you can change that
            effect=bundle.getDouble("key_name8",0);
            crateTime=bundle.getDouble("key_name9",0); //0 is the default value , you can change that
            textPass.setText(dataPassword);
        }else{
            Log.d("__________","Not Data");
        }

        if(dataPassword != null) {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        bundleData.putString("key1", dataPassword);
                        bundleData.putInt("key2", pos);
                        bundleData.putInt("key3", pos2);
                        cal.setArguments(bundleData);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, cal)
                                .commit();
                    } catch (Exception error1) {
                        Toast.makeText(getActivity().getApplicationContext(), "Press insert Password and Select Option", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }else{
            Log.d("__________","55555555555555555555555555555555");
        }

        Chart(times,ef,compex,entropy);
        // ProgressBar
        progreComple = viewpage.findViewById(R.id.proCrack);
        progreTime = viewpage.findViewById(R.id.proTime);
        progrePCI = viewpage.findViewById(R.id.proPCI);
        progreEntro = viewpage.findViewById(R.id.proEntropy);
        // TextView
        progTextC = viewpage.findViewById(R.id.proCrackText);
        progTextT = viewpage.findViewById(R.id.proTimeText);
        progTextP = viewpage.findViewById(R.id.proPCIText);
        progTextE = viewpage.findViewById(R.id.proEntropyText);
        nameC = viewpage.findViewById(R.id.crack);
        nameT = viewpage.findViewById(R.id.times);
        nameP = viewpage.findViewById(R.id.PCI);
        nameE = viewpage.findViewById(R.id.entropy);
        calCP = viewpage.findViewById(R.id.proCrackTextCal);
        calEF = viewpage.findViewById(R.id.proPCITextCal);
        calEP = viewpage.findViewById(R.id.proEntropyTextCal);
        calT = viewpage.findViewById(R.id.proTimeTextCal);
        // Set and Show New Data Time
        if(times != 0){
            int intTime = (int)crateTime;
            progTime= checkProgress(times);
            timeText = checkLevel(times);
            progTextT.setText(timeText);
            progreTime.setProgress(progTime);
            calT.setText(" " + timeSring);
        }

        // Set and Show New Data Effect Length
        if(ef != 0){
            int intEF = (int)effect;
            progEF = checkProgress(ef);
            pciText = checkLevel(ef);
            progrePCI.setProgress(progEF);
            progTextP.setText(pciText);
            calEF.setText(intEF + " Length");
        }
        // Set and Show New Data Complexity
        if(compex != 0){
            progCP = checkProgress(compex);
            compleText = checkLevel(compex);
            progTextC.setText(compleText);
            progreComple.setProgress(progCP);
            calCP.setText(dataCa);
        }
        // Set and Show New Data Entropy
        if(entropy != 0){
            progEP = checkProgress(entropy);
            entroText = checkLevel(entropy);
            progreEntro.setProgress(progEP);
            progTextE.setText(entroText);
            calEP.setText(dataEntro);
        }

        return viewpage;
    }

    public void Chart(int time, int pci, int comple, int entro){
        chart = viewpage.findViewById(R.id.radarChart);
        chart.setBackgroundColor(Color.rgb(255, 255, 255));

        chart.getDescription().setEnabled(false);
        chart.setScaleY(1.2f);
        chart.setScaleX(1.2f);
        chart.setWebLineWidth(1f);
        chart.setWebColor(Color.LTGRAY);
        chart.setWebLineWidthInner(2f);
        chart.setWebColorInner(Color.LTGRAY);
        chart.setWebAlpha(200);

        RadarChart radarChart = viewpage.findViewById(R.id.radarChart);
        ArrayList<RadarEntry> visitorsForFirstWebsite = new ArrayList<>();
        visitorsForFirstWebsite.add(new RadarEntry(pci));
        visitorsForFirstWebsite.add(new RadarEntry(comple));
        visitorsForFirstWebsite.add(new RadarEntry(time));
        visitorsForFirstWebsite.add(new RadarEntry(entro));


        RadarDataSet radarDataSetForFirstWebsite = new RadarDataSet(visitorsForFirstWebsite,"");
        radarDataSetForFirstWebsite.setColor(Color.rgb(255, 255, 255));
        radarDataSetForFirstWebsite.setFillColor(Color.rgb(103, 110, 129));
        radarDataSetForFirstWebsite.setValueTextColor(Color.rgb(0,0,0));
        radarDataSetForFirstWebsite.setValueTextSize(15);
        radarDataSetForFirstWebsite.setDrawFilled(true);
        radarDataSetForFirstWebsite.setFillAlpha(90);
        radarDataSetForFirstWebsite.setLineWidth(2f);
        radarDataSetForFirstWebsite.setDrawHighlightCircleEnabled(true);
        radarDataSetForFirstWebsite.setDrawHighlightIndicators(false);

        RadarData radarData = new RadarData();
        radarData.addDataSet(radarDataSetForFirstWebsite);

        //------------------------------------------------------

        //------------------------------------------------------

        String[] lebels = {"Effective length", "Complexity", "Time", "Entropy"};
        chart.animateXY(1400, 1400, Easing.EaseInOutQuad);

        XAxis xAxis = chart.getXAxis();
        xAxis.setLabelCount(5, true);
        xAxis.setDrawLabels(true);
        xAxis.setTextSize(15f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum(5f);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(lebels));
        xAxis.setTextColor(Color.BLACK);

        YAxis yAxis = chart.getYAxis();
        yAxis.setLabelCount(5, true);
        yAxis.setDrawLabels(true);
        yAxis.setTextSize(15f);
        yAxis.setYOffset(0f);
        yAxis.setXOffset(0f);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(5f);
        yAxis.setTextColor(Color.BLACK);

        Legend l = chart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.WHITE);

        radarChart.setData(radarData);
    }

    public void senddata(int compex, String perCP , long dataEp, double dataFullEp, int entropy, double effect, int ef, double crateTime, int times) {
        this.times = times;
        this.ef = ef;
        this.compex = compex;
        this.entropy = entropy;
        this.perCP = perCP;
        this.dataEp = dataEp;
        this.dataFullEp = dataFullEp;
        this.effect = effect;
        this.crateTime = crateTime;
    }

    public String getData(){
        String log = "A: " +times+ " ,B: " + ef +" ,C: " + compex + " ,D:" + entropy +  " ,E:" + perCP + " ,F:" +dataEp+ " ,G:"+dataFullEp+ " ,H:"+effect+ " ,I:"+crateTime;
        return log;
    }

    public String checkLevel(int dLevel){
        String  levelText = "";
        if(dLevel == 1){
            levelText = "Terrible";
        }else if(dLevel == 2){
            levelText = "Weak";
        }else if(dLevel == 3){
            levelText = "Good";
        }else if(dLevel == 4){
            levelText = "Strong";
        }else if(dLevel == 5){
            levelText = "Perfect";
        }else{
            levelText = "Null";
        }
        return levelText;
    }


    public int checkProgress(int dProgre){
        int  levelProgre = 0;
        if(dProgre == 1){
            levelProgre = 20;
        }else if(dProgre == 2){
            levelProgre = 40;
        }else if(dProgre == 3){
            levelProgre = 60;
        }else if(dProgre == 4){
            levelProgre = 80;
        }else if(dProgre == 5){
            levelProgre = 100;
        }else{
            levelProgre = 0;
        }
        return levelProgre;
    }
}
