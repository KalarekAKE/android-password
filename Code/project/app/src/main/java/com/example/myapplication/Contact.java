package com.example.myapplication;

public class Contact {
    //private variables
    int _id;
    int _strength;
    String _timestamp;
    // Empty constructor
    public Contact(){
    }
    // constructor
    public Contact(int id, int strength, String timestamp){
        this._id = id;
        this._strength = strength;
        this._timestamp = timestamp;
    }
    // constructor
    public Contact(int strength, String timestamp){
        this._strength = strength;
        this._timestamp = timestamp;
    }
    // getting ID
    public int getID(){
        return this._id;
    }
    // setting id
    public void setID(int id){
        this._id = id;
    }
    public void setStrength(int strength){
        this._strength = strength;
    }
    // getting strengthValue
    public int getStrength(){
        return this._strength;
    }
    // setting timeStamp
    public String getTimestamp(){
        return this._timestamp;
    }
    public void setTimestamp(String timestamp){
        this._timestamp = timestamp;
    }
}
