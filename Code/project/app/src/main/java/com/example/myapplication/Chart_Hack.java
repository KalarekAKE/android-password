package com.example.myapplication;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.icu.text.UFormat;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.jetbrains.annotations.Nullable;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class Chart_Hack extends Fragment {

    View viewpage;
    ArrayList barEntries;
    BarChart barChart;
    BarData barData;
    BarDataSet barDataSet;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        viewpage = inflater.inflate(R.layout.reprot_chart, container, false);
        final Button clearhack = viewpage.findViewById(R.id.clearhack);
        clearhack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setTitle("Confirm to clear data graph..!!");
                alertDialogBuilder.setIcon(R.drawable.ic_dices);
                alertDialogBuilder.setMessage("Are you sure You want to clear data Graph");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clear();
                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getActivity().getApplicationContext(), "You clicked on cancel", Toast.LENGTH_LONG).show();
                    }
                });
                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        HorizontalBarChart barChart = (HorizontalBarChart ) viewpage.findViewById(R.id.bar_charthack);

        getEntries();
        barDataSet = new BarDataSet(barEntries, "");
        barData = new BarData(barDataSet);
        barChart.setData(barData);
        barDataSet.setColors(Color.rgb(3,218,197));
//        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(8f);


        barChart.setMaxVisibleValueCount(60);
        barChart.setPinchZoom(false);

        barChart.setDrawGridBackground(false);

        XAxis xl = barChart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawAxisLine(true);
        xl.setDrawGridLines(false);
        xl.setGranularity(1f);

        YAxis yl = barChart.getAxisLeft();
        yl.setDrawAxisLine(true);
        yl.setDrawGridLines(false);
        yl.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//        yl.setInverted(true);
        yl.setAxisMaximum(6f);

        YAxis yr = barChart.getAxisRight();
        yr.setDrawAxisLine(true);
        yr.setDrawGridLines(false);
        yr.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        yr.setAxisMaximum(6f);
//        yr.setInverted(true);

        barChart.setFitBars(true);
        barChart.animateY(500);


        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setFormSize(8f);
        l.setXEntrySpace(4f);

        return viewpage;
    }

    private void getEntries() {

        DatabaseHandler db = new DatabaseHandler(getActivity());
        Log.d("Reading: ", "Reading all contacts..");
        final List<Contact> contacts = db.getAllContacts();
        int str , sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0, sum5 = 0, sum6 = 0, sum7 = 0, sum8 = 0, sum9 = 0, sum10 = 0, sum11 = 0, sum12 = 0;
        int count1 = 0, count2 = 0, count3 = 0, count4 = 0, count5 = 0, count6 = 0, count7 = 0, count8 = 0, count9 = 0, count10 = 0, count11 = 0, count12 = 0 ,count = 0;
        int vale1, vale2, vale3, vale4, vale5, vale6, vale7, vale8, vale9, vale10, vale11, vale12;
        for (Contact cn : contacts) {
            str = cn.getStrength();
            String date = cn.getTimestamp();
            String subdate = date.substring(6,7);
            if (subdate.equals("1")){
                sum1 = sum1+str;
                count1 = count1+1;
            }else if(subdate =="2"){
                sum2 = sum2+str;
                count2 = count2+1;
            } else if(subdate =="3"){
                sum3 = sum3+str;
                count3 = count3+1;
            } else if(subdate =="4"){
                sum4 = sum4+str;
                count4 = count4+1;
            } else if(subdate =="5"){
                sum5 = sum5+str;
                count5 = count5+1;
            }else if(subdate =="6"){
                sum6 = sum6+str;
                count6 = count6+1;
            } else if(subdate =="7"){
                sum7 = sum7+str;
                count7 = count7+1;
            }else if(subdate =="8"){
                sum8 = sum8+str;
                count8 = count8+1;
            } else if(subdate =="9"){
                sum9 = sum9+str;
                count9 = count9+1;
            }else if(subdate =="10"){
                sum10 = sum10+str;
                count10 = count10+1;
            }else if(subdate =="11"){
                sum11 = sum11+str;
                count11 = count11+1;
            }else if(subdate =="12"){
                sum12 = sum12+str;
                count12 = count12+1;
            }else{
                count = count+1;
            }
        }
        String log = "Id: "+ sum1 +  "Size" + count1 +"_____"+count +"{{{{{{__";
        Log.d("Name: ", log);
        vale1 = sum1/checkNotZero(count1);
        vale2 = sum2/checkNotZero(count2);
        vale3 = sum3/checkNotZero(count3);
        vale4 = sum4/checkNotZero(count4);
        vale5 = sum5/checkNotZero(count5);
        vale6 = sum6/checkNotZero(count6);
        vale7 = sum7/checkNotZero(count7);
        vale8 = sum8/checkNotZero(count8);
        vale9 = sum9/checkNotZero(count9);
        vale10 = sum10/checkNotZero(count10);
        vale11 = sum11/checkNotZero(count11);
        vale12 = sum12/checkNotZero(count12);




        barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(2f,vale1));
        barEntries.add(new BarEntry(3f, vale2));
        barEntries.add(new BarEntry(4f, vale3));
        barEntries.add(new BarEntry(5f, vale4));
        barEntries.add(new BarEntry(6f, vale5));
        barEntries.add(new BarEntry(7f, vale6));
        barEntries.add(new BarEntry(8f, vale7));
        barEntries.add(new BarEntry(9f, vale8));
        barEntries.add(new BarEntry(10f,vale9));
        barEntries.add(new BarEntry(11f,vale10));
        barEntries.add(new BarEntry(12f,vale11));
        barEntries.add(new BarEntry(13f,vale12));

    }

    public int checkNotZero(int inputNum){
        int nZero = 1;
        if(inputNum == 0){
            nZero = nZero;
        }else{
            nZero = inputNum;
        }
        return nZero;
    }
    public void clear(){
        Contact contact = new Contact();
        DatabaseHandler db = new DatabaseHandler(getActivity());
        db.deleteContact();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }
}