package com.example.myapplication;

import android.util.Log;
import android.widget.TextView;

public class Check_Cracktime {
    double timeSecondDouble;
    public String Cracktime_toString(int passwordLength , double guesses,double guessesP){
        double time = (Math.pow(93,passwordLength))/(guesses*Math.pow(10,guessesP));
        timeSecondDouble = time;
        String logdata = "Time: " + time;
        String settime = "";
        Log.d("Name: ", logdata);
        double cracktime = 0;
        if(time < 60){
            cracktime = time ;
            int A = (int)cracktime;
            settime = Integer.toString(A)+ " s";
        }
        else if(time > 60 && time < 3600) {
            cracktime = time / 60;
            int B = (int)cracktime;
            settime = Integer.toString(B)+ " min";
        }
        else if(time > 3600 && time < 86400) {
            cracktime = (time/60)/60;
            int C = (int)cracktime;
            settime = Integer.toString(C)+ " h";
        }
        else if(time > 86400 && time < 2592000) {
            cracktime = ((time/60)/60)/24;
            int D = (int)cracktime;
            settime = Integer.toString(D)+ " d";
        }
        else if(time > 2592000 && time < 31104000) {
            cracktime = (((time/60)/60)/24)/30;
            int E = (int)cracktime;
            settime = Integer.toString(E)+ " mo";
        }
        else if(time > 31104000) {
            cracktime = ((((time/60)/60)/24)/30)/12;
            int F = (int)cracktime;
            settime = Integer.toString(F)+ " y";
        }

        String logda = "Time: " + cracktime;
        Log.d("Name: ", logda);
        return settime;
    }


    public int dataLevel (){
        int leveldate = 0 ;
//        int datatime = (int)time * (60*60*24);
//        int timeDay = (60*60*24);
//        int timeMount = timeDay *30;
//        int timeYear = (timeDay * 365);
//        int timeFiveYear = (timeYear * 5);
//        int timeTenYear = timeFiveYear * 2;
        if(timeSecondDouble < 15778800){ //6m
            leveldate = 1;
        }else if(timeSecondDouble < 31536000){//1y
            leveldate = 2;
        }else if(timeSecondDouble < 157680000){//5y
            leveldate = 3;
        }else if(timeSecondDouble < 315576000){//10y
            leveldate = 4;
        }else if(timeSecondDouble > 631152000 ){//20y
            leveldate = 5;
        }

        return leveldate;
    }
}
